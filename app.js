const chalk = require('chalk')
const yargs = require('yargs')

const notes = require('./notes.js')

// Create add command
yargs.command({
    command:'add',
    describe:'Add a new note',
    builder:{
        title:{
            describe: 'Note title',
            demandOption: true, // verplicht veld
            type:'string',
        },
        body:{
            describe:'Body of the note.',
            demandOption:true,
            type:'string',
        }
    },
    handler(argv){
        notes.addNote(argv.title,argv.body)
    }
})

// Create remove command
yargs.command({
    command:'remove',
    describe:'Remove a note',
    builder:{
        title:{
            describe:'Note title',
            demandOption:true,
            type:'string'
        }

    },
    handler(argv) {
        notes.removeNote(argv.title)
    }
})

// Create read command
yargs.command({
    command:'read',
    describe:'Read a note',
    builder:{
        title:{
            describe:'Note to read',
            demandOption:true,
            type:'string'
        }

    },
    handler(argv){
        notes.readNote(argv.title)
    }
})

// Create list command
yargs.command({
    command:'list',
    describe:'List all notes',
    handler() {
        notes.listNotes()
    }
})



//console.log(yargs.argv)   // nodig om te laten werken, vervangen door parse om geen 2 keer output te hebben
yargs.parse()

