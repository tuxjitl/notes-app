const fs = require('fs')
const chalk = require('chalk')

const addNote = (title,body) => {

    const notes = loadNotes()

    //check for duplicate titles
    const duplicateNote = notes.find((note) => note.title === title)//stopt bij de eerste die hij vind
    //check if exists
    if(!duplicateNote){

        notes.push({
            title:title,
            body:body
        })
    
        saveNotes(notes)
        console.log(chalk.bold.bgGreen('New note added'))
        
    }else{
        console.log(chalk.bold.bgRed('Duplicate title. No note added.'))
       
    }

}

const saveNotes = (notes) => {

    const dataJSON = JSON.stringify(notes)

    fs.writeFileSync('notes.json',dataJSON)
}

const removeNote = (title) => {
    const notes = loadNotes()

    const notesNotToDelete = notes.filter((note) => note.title !== title)//degene die niet gedelete worden

    //notesnottodelete is altijd <= notes
    if(notesNotToDelete.length === notes.length){
        console.log(chalk.bold.bgRed("There is no note with title:", title,"to delete."))
    }
    else{
        console.log(chalk.bold.bgGreen('Title ',title,' has been deleted' ))
        saveNotes(notesNotToDelete)
    }
}

const readNote = (title) => {

    const notes = loadNotes()
    const noteToRead = notes.find((note) => note.title === title)

    if(noteToRead){
        console.log(`${chalk.bold.bgGreen(noteToRead.title)}`)
        console.log(`${noteToRead.body}`)
    }else{
        console.log(chalk.bold.bgRed('No note found'))
    }
    

}

const listNotes = () => {
    
    console.log(chalk.inverse('Your notes'))
    
    const notes = loadNotes()

    notes.forEach(note => {
        console.log(`Title: ${note.title} ,Body: ${note.body}`)
    });
    
}

const loadNotes = () => {
    
    try {
        
        const databuffer = fs.readFileSync('notes.json')
        const databufferJSON = databuffer.toString()

        return JSON.parse(databufferJSON)

    } catch (error) {
        return []
    }

}
module.exports = {
    addNote:addNote,
    removeNote:removeNote,
    readNote: readNote,
    listNotes:listNotes
}